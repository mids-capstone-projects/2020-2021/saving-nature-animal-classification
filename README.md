# Animal Detection in Camera Trap Images and Videos

## Abstract

We build an effective animal detection model for the NGO Saving Nature for the purpose of providing a solution to largely reduce manual work on image labeling. The dataset provided by Saving Nature was collected through motion sensor camera traps installed across different rainforests worldwide. It includes both image and video formats. The video data is only used as a supplement set of the image data in our solution for the purpose of boosting data size and solving class imbalance. These concerns will talk in more detail later in the paper. The solution we present here only works with image data and it is used to identify classes of animals that appeared in the image. Our current image classification model correctly identifies animals with an accuracy > 98% and our current image detection model correctly identifies classes of animals with an accuracy > 97%. For our binary classification solution, we expect our solution reduces at least 60% of image labeling work for Saving Nature under the circumstance that the future dataset will contain a similar percentage of ghost images. We can successfully filter out all ghost images which makes up about 60% of the whole dataset provided. We expect our multi-class solution to further reduce the manual labeling work as we are able to correctly predict the specific animal class/species.

## Directory Tree
```
.
├── ClassificationModel
│   ├── classification_dataload.py
│   ├── constant.py
│   ├── train.py
│   └── util.py
├── Final White Paper.pdf
├── README.md
└── WebApp
    ├── flaskapp.py
    ├── model_prediction.py
    ├── models
    │   ├── resnet101_binary_20210416.pth
    │   ├── resnet101_class_20210416.pth
    │   └── resnet101_species_20210419.pth
    ├── __pycache__
    │   ├── capstone.cpython-38.pyc
    │   └── model_prediction.cpython-38.pyc
    └── templates
        └── gui.html
```

## Datasets

Camera trap images and videos data used in this project is provided by Saving Nature.


## File description

* **ClassificationModel**: The directory containing the files for training the classification models

*If you want to re-train the models using new data, please modify the file "classification_dataload.py" to make sure all the data will be successfully read by our models before the training starts.  

* **WebApp**: The directory containing the files for building the web application


## Final Presentation Slide
The slides of our final presentation is [here](https://docs.google.com/presentation/d/1pxlqoyCPXtDlD_0yE7zVqkZTOHmkv6aedX29lk5RKzI/edit?usp=sharing
)

## Web App Usage 

**To run the final product, please open simultaneously the following items:**

- Dropbox MIDS/Input_Data, log in here: https://www.dropbox.com/home/MIDS 
- Server (currently running on local server, will update the server address when finalized with our client)

####  Steps:
1. Please upload the images into the **Input_Data** folder.
2. Once all files have been successfully uploaded, go to GUI and click one of three buttons to start running the classification model.
3. In the MIDS parent folder, there will be a subfolder: **Output_Data** created once the model starts.
4. Check the results in the **Output_Data**.
5. Feel free to click go back in your browser to go back and run different classification models.
6. When finished, feel free to move the **Output_Data** to other locations or delete the entire **Output_Data** and delete all files in the  **Input_Data**.

*Please do not remove the **Input_Data** Folder!


# Project Members

**Xinwen Li, Yingyu Fu, Jiayue Xu**, Duke University 
